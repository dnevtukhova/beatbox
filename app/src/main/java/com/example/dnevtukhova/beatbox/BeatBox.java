package com.example.dnevtukhova.beatbox;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BeatBox {
    private  static  final String TAG = "BeatBox";
    private  static final String SOUNDS_FOLDER = "sample_sounds";
    private static final int MAX_SOUNDS = 5;

    private AssetManager mAssets;
    private List <Sound> mSounds = new ArrayList<>();
    private SoundPool mSoundPool;
    private float speedPlayBack=50;

    public  BeatBox (Context context) {
        mAssets = context.getAssets();
        //этот конструктор считается устаревшим
        //но он нужен для обеспечения совместимости для SDK ниже 21 версии
      //  mSoundPool = new SoundPool(MAX_SOUNDS, AudioManager.STREAM_MUSIC, 0);
        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        mSoundPool = new SoundPool.Builder()
                .setAudioAttributes(attributes)
                .build();
        loadSounds();
    }

    public void play (Sound sound, float speedPlayBack) {
        Integer soundId = sound.getSoundId();
        if(soundId ==null) {
            return;
        }
        float rate = (float) speedPlayBack/50;
        if (rate<=0.5) {
            rate = (float) 0.5;
        }
        mSoundPool.play(soundId, 1.0f, 1.0f, 1, 0, rate);
    }

    public void release() {
        mSoundPool.release();
    }

    private  void loadSounds () {
        String [] soundNames;

            try {
                soundNames = mAssets.list(SOUNDS_FOLDER);
                Log.i(TAG, "Found " + soundNames.length + " sounds");
            } catch (IOException e) {
                Log.e(TAG, "Could not list assets", e);
                return;
            }

            for (String filename: soundNames) {
                try {

                    String assetPath = SOUNDS_FOLDER + "/" + filename;
                Sound sound = new Sound(assetPath);
                    load(sound);
                    mSounds.add(sound);
                } catch (IOException e) {
                    Log.e(TAG,"Could not load sound " + filename, e);
                }

            }
    }

    private void load (Sound sound) throws IOException {
        AssetFileDescriptor afd = mAssets.openFd(sound.getAssetpath());
        int soundId = mSoundPool.load(afd, 1);
        sound.setSoundId(soundId);
    }

    public  List<Sound> getSounds() {
        return mSounds;
    }

    public float getSpeedPlayBack() {
        return speedPlayBack;
    }

    public void setSpeedPlayBack(float speedPlayBack) {
        this.speedPlayBack = speedPlayBack;
    }
}



