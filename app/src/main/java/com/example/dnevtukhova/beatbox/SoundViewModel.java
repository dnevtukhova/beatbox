package com.example.dnevtukhova.beatbox;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class SoundViewModel extends BaseObservable {
    private float speedPlayBack;
    private  Sound mSound;
    private BeatBox mBeatBox;

    public SoundViewModel(BeatBox beatBox) {
        this.mBeatBox = beatBox;
    }

    @Bindable
    public String getTitle () {
        return  mSound.getmName();
    }

    public Sound getmSound() {
        return mSound;
    }

    public void setmSound(Sound mSound) {
        this.mSound = mSound;
        notifyChange();
    }

    public void onButtonClicked() {
        mBeatBox.play(mSound, mBeatBox.getSpeedPlayBack());
    }
}
