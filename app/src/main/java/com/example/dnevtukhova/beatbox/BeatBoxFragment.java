package com.example.dnevtukhova.beatbox;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dnevtukhova.beatbox.databinding.FragmentBeatBoxBinding;
import com.example.dnevtukhova.beatbox.databinding.ListItemSoundBinding;

import java.util.List;

public class BeatBoxFragment extends Fragment implements SeekBar.OnSeekBarChangeListener {
    private  BeatBox mBeatBox;
    private TextView playbackSpeed;
    private SeekBar mSeekBar;

    public static BeatBoxFragment newInstance() {
        return  new BeatBoxFragment();
    }

    @Override
    public void onCreate (Bundle SaveInstanceState) {
        super.onCreate(SaveInstanceState);
        setRetainInstance(true);
        mBeatBox =new BeatBox(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentBeatBoxBinding binding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_beat_box, container, false);
        mSeekBar = (SeekBar)binding.getRoot().findViewById(R.id.seekBar);
        mSeekBar.setOnSeekBarChangeListener(this);

        playbackSpeed = (TextView)binding.getRoot().findViewById(R.id.speedTextView);
        playbackSpeed.setText("50");

        binding.recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),3));
        binding.recyclerView.setAdapter(new SoundAdapter(mBeatBox.getSounds()));

        return  binding.getRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBeatBox.release();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        playbackSpeed.setText("Playback Speed: "+ String.valueOf(mSeekBar.getProgress()));
        mBeatBox.setSpeedPlayBack((float)mSeekBar.getProgress());
    }

    private class SoundHolder extends RecyclerView.ViewHolder {
        private ListItemSoundBinding mBinding;

        public SoundHolder(ListItemSoundBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
            mBinding.setViewModel(new SoundViewModel(mBeatBox));
        }

        public void bind (Sound sound) {
            mBinding.getViewModel().setmSound(sound);
            mBinding.executePendingBindings();
        }
    }
    private  class SoundAdapter extends RecyclerView.Adapter<SoundHolder> {
        private List<Sound> mSounds;

        public SoundAdapter (List <Sound> sounds) {
            mSounds=sounds;
        }

        @NonNull
        @Override
        public SoundHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            ListItemSoundBinding binding = DataBindingUtil
                    .inflate(inflater, R.layout.list_item_sound, parent, false);
            return new SoundHolder(binding);
        }

        @Override
        public void onBindViewHolder(@NonNull SoundHolder holder, int position) {
            Sound sound = mSounds.get(position);
            holder.bind(sound);

        }

        @Override
        public int getItemCount() {
            return mSounds.size();
        }
    }


}


