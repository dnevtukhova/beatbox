package com.example.dnevtukhova.beatbox;

import androidx.annotation.IntegerRes;

public class Sound {
    private String mAssetpath;
    private String mName;
    private Integer mSoundId;

    public Sound(String assetPath) {
        mAssetpath = assetPath;
        String[] components = assetPath.split("/");
        String filename = components[components.length-1];
        mName = filename.replace(".wav", "");
    }

    public  String getAssetpath() {
        return mAssetpath;
    }

    public String getmName() {
        return mName;
    }

    public Integer getSoundId() {
        return mSoundId;
    }

    public void setSoundId(Integer soundId) {
        this.mSoundId = soundId;
    }
}
